from flask import request
from flask_restful import Resource
from flasgger import swag_from
from flasgger import validate
from flask_jwt_extended import create_access_token


class Auth(Resource):
    """
        Class used to authentication.
    """
    yaml_post = 'swagger/auth/post.yml'

    def __init__(self):
        pass

    @swag_from(yaml_post)
    def post(self):
        body = request.json
        token = create_access_token(identity=body)
        return {'token': token}, 201

