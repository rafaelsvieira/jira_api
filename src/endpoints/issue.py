from os import environ
from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flasgger import validate
from flasgger import swag_from
from jira import JIRA
from jira.exceptions import JIRAError
from common.reponse import return_error 
from requests.exceptions import ConnectionError


class Issue(Resource):
    """
        Class use to manipulate the issues from Jira
    """
    yaml_update = 'swagger/issue/update.yml'

    def __init__(self):
        self.jira_url = environ['JIRA_URL']

    @jwt_required
    @swag_from(yaml_update)
    def put(self):
        body = request.json
        validate(body, 'IssueObj', self.yaml_update)
        issue_id = body['id']
        issue_status = body['status']
        auth = get_jwt_identity()
        user = auth['user']
        password = auth['password']

        try:
            jira = JIRA(self.jira_url, auth=(user, password))
        except (JIRAError, RecursionError, ConnectionError) as error:
            msg = '{} - {}'.format('Jira login failed', error)
            return return_error(msg, body, request.headers, user, 403)

        try:
            issue = jira.issue(issue_id)
            jira.transition_issue(issue, transition=issue_status)
            issue = jira.issue(issue_id)
        except JIRAError as error:
            msg = '{} - {}'.format('Fail issue update', error)
            return return_error(msg, body, request.headers, user, 404)

        return issue.raw, 201

