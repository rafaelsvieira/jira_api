from os import environ
from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flasgger import swag_from
from jira import JIRA
from jira.exceptions import JIRAError
from common.reponse import return_error
from requests.exceptions import ConnectionError


class Workflow(Resource):
    """
        Class used to get Jira workflow from a issue.
    """
    yaml_get = 'swagger/workflow/get.yml'

    def __init__(self):
        self.jira_url = environ['JIRA_URL']

    @jwt_required
    @swag_from(yaml_get)
    def get(self, issue_id):
        auth = get_jwt_identity()
        user = auth['user']
        password = auth['password']

        try:
            jira = JIRA(self.jira_url, auth=(user, password))
        except (JIRAError, RecursionError, ConnectionError) as error:
            msg = '{} - {}'.format('Jira login failed', error)
            return return_error(msg, issue_id, request.headers, user, 403)

        try:
            issue = jira.issue(issue_id)
            transitions = jira.transitions(issue)
        except JIRAError as error:
            msg = '{} - {}'.format('Fail issue update', error)
            return return_error(msg, issue_id, request.headers, user, 404)

        return {'transitions': transitions}, 200

