from os import environ
from json import loads
from traceback import format_exc
from botocore.exceptions import ClientError
from boto3.session import Session


class AWSConnect:
    def __init__(self, region=None):
        if region is None:
            self.region = environ["AWS_DEFAULT_REGION"]
        else:
            self.region = region

        self.service_name = "secretsmanager"

    def get_token(self, secret_name):
        try:
            session = Session()
            client = session.client(
                service_name=self.service_name,
                region_name=self.region
            )
            secret = client.get_secret_value(
                SecretId=secret_name
            )
            return loads(secret['SecretString'])['token']
        except ClientError as error:
            trace = format_exc()
            message = f"[ERROR] {error}\nClientError trace: {trace}\n"
            print(message)
            raise Exception(message)
