from flask import Flask
from flask_restful import Api
from flasgger import Swagger
from endpoints.auth import Auth
from endpoints.issue import Issue
from endpoints.workflow import Workflow
from flask_jwt_extended import JWTManager


def main():
    app = Flask(__name__)
    app.config['JWT_SECRET_KEY'] = 'super-secret'
    app.config['SWAGGER'] = {
        'title': 'DevOps Jira API',
        'uiversion': 3,
        'version': '0.1.0',
        'description': (
            "Follow the HTTP Status code from "
            "https://developer.mozilla.org/en-US/docs/Web/HTTP/Status"
        )
    }
    JWTManager(app)
    Swagger(app)
    api = Api(app)
    api.add_resource(Auth, '/auth', endpoint='auth')
    api.add_resource(Issue, '/issue', endpoint='issue')
    api.add_resource(Workflow, '/issue/workflow/<string:issue_id>', endpoint='workflow')
    app.run(debug=True, threaded=True, host='0.0.0.0')

if __name__ == '__main__':
    main()

