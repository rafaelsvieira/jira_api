"""
    This modules implement the common response objects.
"""
def return_error(msg, param, headers, user, status):
    result = {}
    result['msg'] = msg
    result['parameters'] = param
    result['headers'] = dict(headers)
    result['user'] = user
    return result, status

