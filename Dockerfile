FROM amazonlinux:2
ENV DEPENDENCIES_PATH=bucket
ADD $DEPENDENCIES_PATH/pki.tar.gz /etc/
ENV CERT_ROOT_ITAU=/etc/pki/ca-trust/source/anchors/ca-interna-itau.cer
COPY dep_image.txt /app/
WORKDIR /app
RUN yum -y update
RUN yum install -y $(cat dep_image.txt)
RUN rm -f /etc/localtime && touch ~/.bashrc
RUN ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
COPY requirements.txt /app/
RUN pip3 install -r requirements.txt
ADD src/ app/
EXPOSE 5000
ENTRYPOINT ["python3", "app/app.py"]
