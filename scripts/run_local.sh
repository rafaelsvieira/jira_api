#!/bin/bash
BASE_DIR=$(dirname $(readlink -f $0))
VENV_PATH="$BASE_DIR/../.venv"

python3 -m venv $VENV_PATH
source $VENV_PATH/bin/activate
pip install -r $BASE_DIR/../requirements.txt

echo -e "Run the command on project root path:\n\n$ source .venv/bin/activate\n\nAnd after:\n"
echo -e "$ python src/app_run.py\n"
